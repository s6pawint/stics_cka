setwd(dirname(rstudioapi::getActiveDocumentContext()$path))

library("readxl")
library(lubridate)

# Read the Weather Data form CKA of 2023
weather <- data.frame(read_excel("./CKA2023_01-12 V2.xlsx", sheet="summary"))

# Convert the unit of the global Radiation from kWh/m2 to J/m2
# 1 J = 1 Ws = 1/(60*60) Wh = 10^-3/(60*60) kWh = 1/3600000 kWh
# <=> 1 kWh = 3600000 J = 3.6 MJ
# => 1kWh/m2 = 3.6 MJ/m2
# converting factor kWh to MJ:  3.6 MJ/ 1 kWh

weather$GlobalRadiation_MJ.m.2 <- 3.6 * weather$GlobalRadiation_kWh.m.2

head(weather)

# Convert the Date column from string into the R specific Date format
weather$Date <- as.Date(weather$Date)

#Add coulmn for the year
weather$Year <- year(weather$Date)

# Add column for the month
weather$Month <- month(weather$Date)

# Add column for the day of in month
weather$Day <- day(weather$Date)

# reorder the data frame to make the assignment of the columns in the STICS Interface easier
columnOrder <- c("Year", "Julian_day", "Month", "Day", "Temp_min_C", "Temp_max_C", "GlobalRadiation_MJ.m.2", "rainfall_mm", "WindSpeed_m.2.1", "RelativeHumidity_p")
data.frame(columnOrder, index =(1:length(columnOrder)))

outputTable <- weather[,columnOrder]

head(outputTable)

weather$GlobalRadiation_kWh.m.2*3.6

write.table(outputTable, "./WeatherCKA.csv", row.names=F, sep = ";")






