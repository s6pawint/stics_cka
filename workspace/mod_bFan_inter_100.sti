 Balance of the STICS simulation V10.0.0_r3391_2022-10-26            , model culture
 **************************************************************
 1. INPUT DATA
 *******************
   Weather file           :                                               CKA
   Cropping practices file:                     CKA_Fanfare_inter_100_tec.xml
   Plant file             :                                 faba_bean_plt.xml
   Cultivar: Fanfare         variety group = 1
   Initialisations file   :                                  CKA_2023_ini.xml
   Initial soil values    : CKA_2023_202
      Z (cm)      Water (%)   NO3 (kg/ha)    NH4 (kg/ha)
        10.        30.4          11.9           3.0
         5.        29.6          10.7           1.5
        15.        25.2          21.4           3.0
        30.        22.7          49.3           7.7
        30.        21.1          39.6           4.2

   Start of simulation   : 29-apr-2023      day 119
   End of simulation     : 31-dec-2023      day 365   (or 365)
   Irrigation read 
   Fertilisation read
   Method of PET calculation:            Penman calculated

   Irrigation
   No irrigation during the period

   Fertilisation:        Number of applications = 1
          date      amount (kg N/ha)  fertilizer type
       22-may-2023         77.               1

   Organic residues and/or soil tillage
         No application, no tillage

 2. CROP DEVELOPMENT
 *******************
     type                   : short day plant
     development driven by photo-thermal units
     temperature used : air temperature

        stage            BBCH       date         units  cumulative units  lai
   ------------                -----------  -----  ----------------  ---
     sowing              -99    3-may-2023        0.        0.
   Vegetative stages
     ger calculated      -99    8-may-2023       57.       57.       0.0
     lev calculated      -99   23-may-2023      157.      214.       0.0
     amf calculated      -99   30-jul-2023      101.      315.       0.2
     lax calculated      -99   22-aug-2023      218.      533.       1.7
     lan calculated            22-aug-2023        0.      533.       1.7
   Reproductive stages
     flo calculated      -99   22-aug-2023      533.      533.
     drp calculated      -99   22-aug-2023        0.      533.       1.7
     mat calculated      -99   22-aug-2023        0.      533.       1.7
     crop harvest        99    22-aug-2023        0.      533.       1.7
     debdes calculated   -99   22-aug-2023        0.      533.       1.7

   Length of cycle =111 days
   Harvest operation: whole plant harvested
   Harvest objective: according to water content

 Attention: for this simulation the date of harvest is a finishing date
 the sum of units from sowing till harvest can not have been reached on this date

 3. GROWTH AND COMPONENTS OF YIELD
 **********************************
     22-aug-2023
   Aerial biomass at harvest (0% water)=    1.33 t/ha
   Grain or fruit Yield   (0% water)   =    0.00 t/ha
   Grain or fruit Yield ( 0.% water)   =    0.00 t/ha

   Senescent aerial biomass (0% water) =    0.00 t/ha

   Number of grains or fruits          =      0. /m2
   Plant density                       =    15.3 /m2
   Grain or fruit weight ( 0.% water)  =     0.000 g
   Growth rate (lag phase)             =     0.0 mg/m2/j
   Number of emerged leaves            =    17
   Number of hot or cold days          =     0

   Quantity of N in the crop           =    94. kg/ha
   Quantity of N in grains or fruits   =     0. kg/ha
   Quantity of N in roots              =    53. kg/ha

   N content of whole plant            =  7.04 % DM

   Nitrogen fertiliser efficiency      = 0.71

 4. WATER and NITROGEN BALANCE over the crop life
 ************************************************
     Cumulative ETMax (eos+eop)     =  169. mm
     Cumulative AET                 =  169. mm
     Cumulative Evaporation         =  145. mm
     Cumulative Transpiration       =   25. mm
     Cumulative Rainfall+irrigation =  246. mm

     Water reserve used by plant    =   25. mm
     Maximum rooting depth          =   90. cm

   Mean STRESS indices :              swfac    turfac    inns   tcult-tair  exofac    etr/etm   etm/etr
   vegetative phase    (lev-drp)      1.00      1.00      1.00      2.37      0.00      1.00      1.00
   reproductive phase  (drp-mat)      1.00      1.00      1.00      0.39      0.00      1.00      1.00

    Frost damage on leaves : before AMF     0.%    after AMF  79.%

 5. WATER, NITROGEN, CARBON BALANCE over the whole simulation period  (247 days)
 *******************************************************************************
   Normalised days at 15.degree C:    Humus = 299.      Residues =   0.
   Potential mineralisation rate = 0.47 kg N/ha/day   or 3.51% per year
WATER BALANCE  (mm)
     initial water content    255.        final water content      196.
     rain                     470.        evaporation              209.
     irrigation                 0.        transpiration             25.
     capillary rise            -0.        runoff                     0.
                                          deep infiltration        294.
                                          mole drainage              0.
                                          leaf interception          0.
                                          mulch interception         2.
                                          ineffective irrigation     0.
     -----------------------------        -----------------------------
     TOTAL INITIAL            726.        TOTAL FINAL              726.

PLANT NITROGEN BALANCE (kg N/ha)
     ------ Initial pools ------         ------- Final pools -------
     Aerial plant N            0.                                   0.
     Root N                    0.                                   0.
     Perennial reserves N      0.                                   0.
     Total plant N             0.                                   0.
     ------- Input fluxes ------         ------ Output fluxes ------
     N uptake                 93.        N exported                61.
     N fixation                1.        N returned to soil        86.
     ----------------------------        ----------------------------
     TOTAL INITIAL            94.        TOTAL FINAL              147.


SOIL MINERAL NITROGEN BALANCE (kg N/ha)
     ------ Initial pools ------         ------- Final pools -------
     NH4-N                    19.                                  14.
     NO3-N                   133.                                  93.
     Total mineral N         152.                                 107.
     ------- Input fluxes ------         ------ Output fluxes ------
                                         plant N uptake            93.
     rain                      9.        leaching                 189.
     irrigation                0.        leaching in mole drains    0.
     fertiliser               77.        fertiliser immobilisat.   16.
                                         fertiliser volatilisat.    6.
     humus mineralisation    140.        manure N volatilisat.      0.
     residue mineralisation   33.        (N2+N2O)-N losses          1.
     ----------------------------        ----------------------------
     TOTAL INITIAL           412.        TOTAL FINAL              412.

SOIL ORGANIC NITROGEN BALANCE (kg N/ha)
     ------ Initial pools ------         ------- Final pools -------
     Active humified          1745.                              1642.
     Inert humified           3241.                              3241.
     Zymogeneous biomass         0.                                31.
     Organic residues            0.                                 0.
     Deep root residues          0.                                 0.
     Mulch residues              0.                                 0.
     Total organic pools      4986.                              4914.
     ------- Input fluxes ------         ------ Output fluxes ------
     fertiliser immobilis.      16.
     organic fertiliser          0.
     crop residues              33.
     fallen leaves               0.      Priming (PE)               0.
     trimmed leaves              0.      Mineralisation - PE      174.
     dead roots                 53.
     dead perennial organs       0.
     ----------------------------        ----------------------------
     TOTAL INITIAL            5088.      TOTAL FINAL             5088.

SOIL ORGANIC CARBON BALANCE   (kg C/ha)
     ------ Initial pools ------         ------- Final pools -------
     Active humified         16620.                             15497.
     Inert humified          30866.                             30866.
     Zymogeneous biomass         0.                               242.
     Organic residues            0.                                 0.
     Deep root residues          0.                                 0.
     Mulch residues              0.                                 2.
     Total organic pools     47486.                             46606.
     ------- Input fluxes ------         ------ Output fluxes ------
     fertiliser immobilis.       0.
     organic fertiliser          0.
     crop residues             196.
     fallen leaves               0.      Priming (PE)               0.
     trimmed leaves              0.      Mineralisation - PE     1742.
     dead roots                666.
     dead perennial organs       0.
     ----------------------------        ----------------------------
     TOTAL INITIAL           48348.      TOTAL FINAL            48348.

Heterotrophic Respiration (kg C/ha): Residues =  414.
                                     Humus    = 1328.
                                     Total    = 1742.

Cumulative N2O emissions  (kg N/ha): nitrification      =  0.48
                                     denitrification    =  0.28
                                     Total              =  0.75

Aboveground CROP RESIDUES returned to soil:            DM =  0.5 t/ha   C content =  196. kg/ha   N content =   33. kg/ha   C/N =  6.0
Total CROP RESIDUES returned to soil:stubble+roots     DM =  2.1 t/ha   C content =  862. kg/ha   N content =   86. kg/ha   C/N = 10.0
