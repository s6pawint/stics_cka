 Balance of the STICS simulation V10.0.0_r3391_2022-10-26            , model culture
 **************************************************************
 1. INPUT DATA:       INTERCROPPING
 **********************************
   Weather file           :                                               CKA
   Cropping practices file:                        CKA_Anabel_inter_0_tec.xml
   Plant file             :                              spring_wheat_plt.xml
   Cultivar: Anabel          variety group =10
   Initialisations file   :                            CKA_2023_inter_ini.xml
   Initial soil values    : CKA_2023_202
      Z (cm)      Water (%)   NO3 (kg/ha)    NH4 (kg/ha)
        10.        30.4          11.9           3.0
         5.        29.6          10.7           1.5
        15.        25.2          21.4           3.0
        30.        22.7          49.3           7.7
       140.        21.1         184.8          19.4

   Start of simulation   : 29-apr-2023      day 119
   End of simulation     : 31-dec-2023      day 365   (or 365)
   Irrigation read 
   Fertilisation read
   Method of PET calculation:     Shuttleworth and Wallace

   Irrigation
   No irrigation during the period
   No fertilisation during the period

   Organic residues and/or soil tillage
         No application, no tillage

 2. CROP DEVELOPMENT
 *******************
     type                   : long-day plant
     development driven by photo-thermal units
     temperature used : crop temperature

        stage            BBCH       date         units  cumulative units  lai
   ------------                -----------  -----  ----------------  ---
     sowing              00     3-may-2023        0.        0.
   Vegetative stages
     ger calculated      05     7-may-2023       67.       67.       0.0
     lev calculated      09    12-may-2023       73.      140.       0.0
     amf calculated      35    22-aug-2023      124.      264.       0.1
     lax calculated      55    22-aug-2023        0.      264.       0.1
     lan calculated            22-aug-2023        0.      264.       0.1
   Reproductive stages
     flo calculated      65    22-aug-2023      264.      264.
     drp calculated      71    22-aug-2023        0.      264.       0.1
     mat calculated      89    22-aug-2023        0.      264.       0.1
     crop harvest        99    22-aug-2023        0.      264.       0.1
     debdes calculated   75    22-aug-2023        0.      264.       0.1

   Length of cycle =111 days
   Harvest operation: whole plant harvested
   Harvest objective: according to water content

 Attention: for this simulation the date of harvest is a finishing date
 the sum of units from sowing till harvest can not have been reached on this date

 3. GROWTH AND COMPONENTS OF YIELD
 **********************************
     22-aug-2023
   Aerial biomass at harvest (0% water)=    0.01 t/ha
   Grain or fruit Yield   (0% water)   =    0.00 t/ha
   Grain or fruit Yield ( 0.% water)   =    0.00 t/ha

   Senescent aerial biomass (0% water) =    0.00 t/ha

   Number of grains or fruits          =      0. /m2
   Plant density                       =   120.0 /m2
   Grain or fruit weight ( 0.% water)  =     0.000 g
   Growth rate (lag phase)             =     0.0 mg/m2/j
   Number of emerged leaves            =     1
   Number of hot or cold days          =     0

   Quantity of N in the crop           =     0. kg/ha
   Quantity of N in grains or fruits   =     0. kg/ha
   Quantity of N in roots              =     1. kg/ha

   N content of whole plant            =  8.50 % DM

 4. WATER and NITROGEN BALANCE over the crop life
 ************************************************
     Cumulative ETMax (eos+eop)     =  192. mm
     Cumulative AET                 =  192. mm
     Cumulative Evaporation         =  192. mm
     Cumulative Transpiration       =    0. mm
     Cumulative Rainfall+irrigation =  246. mm

     Water reserve used by plant    =   31. mm
     Maximum rooting depth          =   35. cm

   Mean STRESS indices :              swfac    turfac    inns   tcult-tair  exofac    etr/etm   etm/etr
   vegetative phase    (lev-drp)      1.00      1.00      1.00       NaN      0.00       NaN      1.00
   reproductive phase  (drp-mat)      1.00      1.00      1.00       NaN      0.00       NaN      1.00

 5. WATER, NITROGEN, CARBON BALANCE over the whole simulation period  (247 days)
 *******************************************************************************
   Normalised days at 15.degree C:    Humus =  NaN      Residues =   0.
   Potential mineralisation rate = 0.00 kg N/ha/day   or  NaN% per year
WATER BALANCE  (mm)
     initial water content    531.        final water content      580.
     rain                     470.        evaporation              198.
     irrigation                 0.        transpiration             NaN
     capillary rise            -0.        runoff                     0.
                                          deep infiltration          0.
                                          mole drainage              0.
                                          leaf interception          0.
                                          mulch interception       224.
                                          ineffective irrigation     0.
     -----------------------------        -----------------------------
     TOTAL INITIAL           1002.        TOTAL FINAL               NaN

PLANT NITROGEN BALANCE (kg N/ha)
     ------ Initial pools ------         ------- Final pools -------
     Aerial plant N            0.                                   0.
     Root N                    0.                                   0.
     Perennial reserves N      0.                                   0.
     Total plant N             0.                                   0.
     ------- Input fluxes ------         ------ Output fluxes ------
     N uptake                  1.        N exported                 0.
     N fixation                0.        N returned to soil         1.
     ----------------------------        ----------------------------
     TOTAL INITIAL             1.        TOTAL FINAL                1.


SOIL MINERAL NITROGEN BALANCE (kg N/ha)
     ------ Initial pools ------         ------- Final pools -------
     NH4-N                    35.                                  NaN
     NO3-N                   278.                                 272.
     Total mineral N         313.                                  NaN
     ------- Input fluxes ------         ------ Output fluxes ------
                                         plant N uptake             1.
     rain                      9.        leaching                   0.
     irrigation                0.        leaching in mole drains    0.
     fertiliser                0.        fertiliser immobilisat.    0.
                                         fertiliser volatilisat.    0.
     humus mineralisation     NaN        manure N volatilisat.      0.
     residue mineralisation   NaN        (N2+N2O)-N losses         23.
     ----------------------------        ----------------------------
     TOTAL INITIAL            NaN        TOTAL FINAL               NaN

SOIL ORGANIC NITROGEN BALANCE (kg N/ha)
     ------ Initial pools ------         ------- Final pools -------
     Active humified          1745.                                NaN
     Inert humified           3241.                              3241.
     Zymogeneous biomass         0.                                NaN
     Organic residues            0.                                 0.
     Deep root residues          0.                                 0.
     Mulch residues              0.                                NaN
     Total organic pools      4986.                                NaN
     ------- Input fluxes ------         ------ Output fluxes ------
     fertiliser immobilis.       0.
     organic fertiliser          0.
     crop residues               0.
     fallen leaves               0.      Priming (PE)               0.
     trimmed leaves              0.      Mineralisation - PE       NaN
     dead roots                  1.
     dead perennial organs       0.
     ----------------------------        ----------------------------
     TOTAL INITIAL            4987.      TOTAL FINAL               NaN

SOIL ORGANIC CARBON BALANCE   (kg C/ha)
     ------ Initial pools ------         ------- Final pools -------
     Active humified         16620.                                NaN
     Inert humified          30866.                             30866.
     Zymogeneous biomass         0.                                NaN
     Organic residues            0.                                 0.
     Deep root residues          0.                                 0.
     Mulch residues              0.                                NaN
     Total organic pools     47486.                                NaN
     ------- Input fluxes ------         ------ Output fluxes ------
     fertiliser immobilis.       0.
     organic fertiliser          0.
     crop residues               1.
     fallen leaves               0.      Priming (PE)               0.
     trimmed leaves              0.      Mineralisation - PE       NaN
     dead roots                 26.
     dead perennial organs       0.
     ----------------------------        ----------------------------
     TOTAL INITIAL           47512.      TOTAL FINAL               NaN

Heterotrophic Respiration (kg C/ha): Residues =   NaN
                                     Humus    =   NaN
                                     Total    =   NaN

Cumulative N2O emissions  (kg N/ha): nitrification      =  0.04
                                     denitrification    =  9.79
                                     Total              =  9.84

Aboveground CROP RESIDUES returned to soil:            DM =  0.0 t/ha   C content =    1. kg/ha   N content =    0. kg/ha   C/N =  4.7
Total CROP RESIDUES returned to soil:stubble+roots     DM =  0.1 t/ha   C content =   26. kg/ha   N content =    1. kg/ha   C/N = 37.4
